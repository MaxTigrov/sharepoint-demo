﻿using Microsoft.Exchange.WebServices.Data;
using System;

namespace Demo.EWS
{
    /// <summary>
    /// Билдер события для календаря outlook.
    /// </summary>
    internal class AppointmentBuilder
    {
        #region [fields]

        private Appointment _result;

        private ExchangeService _exchangeService;

        #endregion [fields]

        public AppointmentBuilder(ExchangeService service)
        {
            _exchangeService = service;

            Reset();
        }

        #region [methods]

        /// <summary>
        /// Сбросить все параметры.
        /// </summary>
        public AppointmentBuilder Reset()
        {
            _result = new Appointment(_exchangeService);

            return this;
        }

        /// <summary>
        /// Устанавливает тему события.
        /// </summary>
        public AppointmentBuilder SetSubject(string subject)
        {
            _result.Subject = subject;

            return this;
        }

        /// <summary>
        /// Устанавливает текст события.
        /// </summary>
        public AppointmentBuilder SetBody(string body)
        {
            _result.Body = body;

            return this;
        }

        /// <summary>
        /// Устанавливает дату начала события.
        /// </summary>
        public AppointmentBuilder SetStartDateTime(DateTime start)
        {
            _result.Start = start;

            return this;
        }

        /// <summary>
        /// Устанавливает дату окончания события.
        /// </summary>
        public AppointmentBuilder SetEndDateTime(DateTime end)
        {
            _result.End = end;

            return this;
        }

        /// <summary>
        /// Устанавливает место события.
        /// </summary>
        public AppointmentBuilder SetLocation(string location)
        {
            _result.Location = location;

            return this;
        }

        /// <summary>
        /// Возвращает результат сформированного события.
        /// </summary>
        public Appointment Result()
        {
            return _result;
        }

        #endregion [methods]
    }
}
