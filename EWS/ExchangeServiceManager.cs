﻿using Microsoft.Exchange.WebServices.Data;
using Microsoft.SharePoint;
using System;
using System.Net;

namespace Demo.EWS
{
    internal class ExchangeServiceManager
    {
        #region [consts]

        private const string AutodiscoverUrl = "";

        private const string UserLogin = "";

        private const string UserPassword = "";

        #endregion [consts]

        #region [fields]

        private ExchangeService _exchangeService;

        #endregion [fields]

        #region [properties]

        public ExchangeService Service
        {
            get { return _exchangeService; }
        }

        public string ErrorMessage { get; private set; }

        #endregion [properties]

        public ExchangeServiceManager()
        {
            _exchangeService = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
            NetworkCredential credentials = new NetworkCredential(UserLogin, UserPassword);

            _exchangeService.UseDefaultCredentials = false;
            _exchangeService.Credentials = credentials;

            _exchangeService.AutodiscoverUrl(AutodiscoverUrl);
        }

        #region [methods]

        public void SetImpersonateUser(SPUser user)
        {
            _exchangeService.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, user.Email);
        }

        public bool SendApointment(Appointment appointment)
        {
            bool result = true;

            try
            {
                appointment.Save(SendInvitationsMode.SendToNone);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
                result = false;
            }

            return result;
        }

        #endregion [methods]
    }
}
