﻿using Demo.Repositories.DataConverters;
using kirtomoto.EF.Entities;
using kirtomoto.EF.Entities.SPFieldDataConverters;
using Microsoft.SharePoint;
using System;
using System.Collections.Generic;

namespace Demo.Repositories.EventsDictionary
{
    /// <summary>
    /// Модель сущности мероприятия.
    /// </summary>
    public class EventEntity : Entity
    {
        #region [consts]

        public const string FieldName_EventCategory = "EventCategory";
        public const string FieldName_EventDateTime = "EventDateTime";
        public const string FieldName_EventTime = "EventTime";
        public const string FieldName_MaxParticipants = "MaxParticipants";
        public const string FieldName_EventParticipants = "EventParticipants";

        #endregion [consts]

        #region [properties]

        /// <summary>
        /// Название мероприятия.
        /// </summary>
        public string EventTitle { get; set; }

        /// <summary>
        /// Категория события.
        /// </summary>
        [Field(SPFieldType.Choice, FieldName = FieldName_EventCategory)]
        public string EventCategory { get; private set; }

        /// <summary>
        /// Дата события.
        /// </summary>
        [Field(SPFieldType.DateTime, FieldName = FieldName_EventDateTime)]
        public DateTime EventDate { get; set; }

        /// <summary>
        /// Время события.
        /// </summary>
        [Field(SPFieldType.MultiChoice, FieldName = FieldName_EventTime)]
        [FieldConverter(typeof(MultiChoiseDataConverter))]
        public List<string> EventTime { get; set; }

        /// <summary>
        /// Максимальное кол-во участников.
        /// </summary>
        [Field(SPFieldType.Integer, FieldName = FieldName_MaxParticipants)]
        public int MaxParticipants { get; set; }

        /// <summary>
        /// Уже зарегистрированные участники.
        /// </summary>
        [Field(SPFieldType.Note, FieldName = FieldName_EventParticipants)]        
        public string EventParticipants { get; set; }

        #endregion [properties]

        #region [methods]

        protected override void BySPListItem(SPListItem item)
        {
            EventTitle = item.Title;
            base.BySPListItem(item);
        }

        protected override void ToSPListItem(SPListItem item)
        {
            item[SPBuiltInFieldId.Title] = EventTitle;
            base.ToSPListItem(item);
        }

        #endregion[methods]
    }
}
