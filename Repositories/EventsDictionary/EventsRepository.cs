﻿using kirtomoto.EF.Repositories;
using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Demo.Repositories.EventsDictionary
{
    /// <summary>
    /// Репозиторий для работы со справочником мероприятий.
    /// </summary>
    [SPListInfo("EventsDictionary", ListTitle = "Справочник мероприятий")]
    public sealed class EventsRepository : Repository<EventEntity>
    {
        #region [properties]

        protected override int ListTemplateType
        {
            get
            {
                return 30001;
            }
        }

        #endregion [properties]

        public EventsRepository(Guid listId)
            : base(listId)
        {
        }

        public EventsRepository(SPWeb web)
            : base(web)
        {
        }

        public EventsRepository(SPWeb web, Guid listId)
            : base(web, listId)
        {
        }

        #region [methods]

        /// <summary>
        /// Получить список категорий мероприятий.
        /// </summary>
        public List<string> GetEventCategories()
        {
            List<string> result = new List<string>();
            if (SPList != null && SPList.Fields.ContainsField(EventEntity.FieldName_EventCategory))
            {
                SPField field = SPList.Fields.GetField(EventEntity.FieldName_EventCategory);
                if (field != null && field is SPFieldChoice)
                {
                    StringCollection choices = (field as SPFieldChoice).Choices;
                    if (choices != null)
                    {
                        result = choices.OfType<string>().ToList();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Получить список мероприятий по категории.
        /// </summary>
        public List<EventEntity> GetByEventCategory(string category)
        {
            return GetListItems().Where(w => w.EventCategory.Equals(category)).ToList();
        }

        /// <summary>
        /// Получить событие по параметрам или null.
        /// </summary>
        public EventEntity Get(string category, string title, DateTime date, TimeSpan time)
        {
            int eventId = GetByEventCategory(category).Select(EventEntityConverter.Convert)
                .Where(w => w.Title.Equals(title, StringComparison.OrdinalIgnoreCase)
                    && w.Date.Date == date.Date && w.Times.Contains(time)
                ).Select(x => x.ID).FirstOrDefault();

            if (eventId > 0)
            {
                return Get(eventId);
            }
            else
            {
                return null;
            }
        }

        #endregion [methods]
    }
}
