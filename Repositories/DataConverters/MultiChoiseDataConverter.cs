﻿using kirtomoto.EF;
using Microsoft.SharePoint;
using SP.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.Repositories.DataConverters
{
    /// <summary>
    /// Конвертер для поля типа MultiChoise.
    /// </summary>
    internal sealed class MultiChoiseDataConverter : ISPFieldDataConverter
    {
        #region [consts]

        private const string Delimeter = ";#";

        #endregion [consts]

        #region [methods]

        /// <summary>
        /// Получает значение из строкового представления и возращает ответ ввиде List<string>.
        /// </summary>
        public object GetFieldValue(string fieldValue, SPWeb web)
        {
            // Значения приходят в виде разделителя ;#8:00;#10:00;#12:00;#
            List<string> result = new List<string>();

            if (!fieldValue.IsNullOrEmpty())
            {
                result = fieldValue.Split(new[] { Delimeter }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            return result;
        }

        /// <summary>
        /// Получает значение из элемента списка и возращает ответ ввиде List<string>. 
        /// </summary>
        public object GetFieldValue(SPListItem item, string fieldName)
        {
            List<string> result = new List<string>();

            if (item.Fields.ContainsField(fieldName) && item[fieldName] != null)
            {
                result = GetFieldValue(item[fieldName].ToString(), item.Web) as List<string>;
            }

            return result;
        }

        /// <summary>
        /// Задает значение в поле элемента списка.
        /// </summary>
        /// <param name="value">IEnumerable<string>.</param>
        public void SetFieldValue(SPListItem item, string fieldName, object value)
        {
            if (item.Fields.ContainsField(fieldName))
            {
                if (value is IEnumerable<string>)
                {
                    string stringValue = string.Join(Delimeter, (value as IEnumerable<string>).Where(w => w != null));
                    item[fieldName] = stringValue;
                }
                else
                {
                    item[fieldName] = null;
                }
            }
        }

        #endregion [methods]
    }
}
