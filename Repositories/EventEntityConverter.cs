﻿using Demo.Models;
using Demo.Repositories.EventsDictionary;
using System;
using System.Linq;

namespace Demo.Repositories
{
    /// <summary>
    /// Конвертер данных из сущности в другие модели.
    /// </summary>
    internal class EventEntityConverter
    {
        #region [properties]

        /// <summary>
        /// Конвертация из сущности во вьюмодель.
        /// </summary>
        public static EventViewModel Convert(EventEntity entity)
        {
            EventViewModel result = null;

            if (entity != null)
            {
                result = new EventViewModel();

                result.ID = entity.ID;
                result.Title = entity.EventTitle;
                result.Date = entity.EventDate;
                result.EventParticipants = entity.EventParticipants;
                result.MaxParticipants = entity.MaxParticipants;
                result.Times = entity.EventTime.Select(x =>
                {
                    TimeSpan time = TimeSpan.Zero;
                    TimeSpan.TryParse(x, out time);
                    return time;
                }).ToList();
            }

            return result;
        }

        #endregion [properties]
    }
}
