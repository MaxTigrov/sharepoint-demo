﻿using Demo.Repositories.EventsDictionary;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using SP.Core;
using System;
using System.Globalization;
using System.Net;
using System.Net.Mail;

namespace Demo.BLL
{
    internal class MailHelper
    {
        #region [fields]

        private EventsRepository _eventsRepository;

        #endregion [fields]

        #region [properties]

        /// <summary>
        /// Текст ошибки.
        /// </summary>
        public string ErrorMessage { get; set; }

        #endregion [properties]

        public MailHelper(EventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }

        #region [methods]

        public bool TrySendEmail(string category, string title, string date, string time, SPUser user)
        {
            bool result = true;

            DateTime dateValue;
            TimeSpan timeValue = TimeSpan.Parse(time);
            DateTime.TryParseExact(date, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue);

            EventEntity entity = _eventsRepository.Get(category, title, dateValue, timeValue);
            if (entity != null)
            {
                if (user.Email.IsNullOrEmpty())
                {
                    result = false;
                    ErrorMessage = "Не указан емейл";
                }
                else
                {
                    DateTime enentDate = dateValue.Date.Add(timeValue);
                    string eventTitle = string.Format(@"{0}_{1}_{2:dd MMMM yyyy}_{3}", entity.EventCategory, entity.EventTitle, dateValue, timeValue.ToString(@"hh\:mm"));
                    string body = eventTitle;

                    //if (!SPUtility.SendEmail(_eventsRepository.Web, false, true, user.Email, eventTitle, body, false))
                    //{
                    //    result = false;
                    //    ErrorMessage = "Не удалось отправить емейл уведомление";
                    //}
                    try
                    {
                        using (SmtpClient client = new SmtpClient("smtp.yandex.ru"))
                        {
                            //client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential("*****@yandex.ru", "******");
                            client.EnableSsl = true;
                            client.Port = 25;

                            client.Send("sharepoint.inline@yandex.ru", user.Email, eventTitle, body);
                        }
                    }
                    catch(Exception ex)
                    {
                        result = false;
                        ErrorMessage = "Не удалось отправить емейл уведомление";
                    }
                }
            }
            else
            {
                result = false;
                ErrorMessage = "Мероприятие, с выбранными параметрами не найдено. Попробуйте обновить страницу";
            }

            return result;
        }

        #endregion [methods]
    }
}
