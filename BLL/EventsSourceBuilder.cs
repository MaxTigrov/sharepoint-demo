﻿using Demo.Models;
using Demo.Repositories;
using Demo.Repositories.EventsDictionary;
using Microsoft.SharePoint;
using SP.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

namespace Demo.Users
{
    /// <summary>
    /// Билдер данных по событиям.
    /// </summary>
    internal sealed class EventsSourceBuilder
    {
        #region [fields]

        private EventsRepository _eventsRepository;

        private List<EventViewModel> _result = new List<EventViewModel>();

        #endregion [fields]
        
        public EventsSourceBuilder(EventsRepository eventsRepository)
        {
            if (eventsRepository == null)
                throw new ArgumentNullException(nameof(eventsRepository));

            _eventsRepository = eventsRepository;
        }

        #region [methods]

        /// <summary>
        /// Получить события по категории.
        /// </summary>
        public EventsSourceBuilder GetEventsByCategory(string category)
        {
            List<EventViewModel> result = new List<EventViewModel>();
            // Все записи приводим к вьюмодели.
            result = _eventsRepository.GetByEventCategory(category).Select(EventEntityConverter.Convert).ToList();
            _result = result;

            return this;
        }

        /// <summary>
        /// Получить события по ID события.
        /// </summary>
        public EventsSourceBuilder GetEventByID(int eventId)
        {
            EventEntity entity = _eventsRepository.Get(eventId);
            if (entity != null)
            {
                _result.Add(EventEntityConverter.Convert(entity));
            }

            return this;
        }

        /// <summary>
        /// Отфильтровать события по дате и времени доступности.
        /// Оставляет только будущие события, на которые можно записаться.
        /// </summary>
        public EventsSourceBuilder FilterEventsDateTime()
        {
            List<EventViewModel> result = new List<EventViewModel>();
            foreach (EventViewModel model in _result)
            {
                if (model.Date < DateTime.Today)
                {
                    continue;
                }

                if (model.Date == DateTime.Today)
                {
                    model.Times = model.Times.Where(x => x > DateTime.Now.TimeOfDay).ToList();
                    if (model.Times.IsNotEmpty())
                    {
                        result.Add(model);
                    }
                    else
                    {
                        continue;
                    }
                }

                result.Add(model);
            }

            _result = result;

            return this;
        }

        /// <summary>
        /// Отфильтровать события по названию.
        /// </summary>
        public EventsSourceBuilder FilterEventsTitle(string eventTitle)
        {
            _result = _result.Where(w => w.Title.Equals(eventTitle, StringComparison.OrdinalIgnoreCase)).ToList();

            return this;
        }

        /// <summary>
        /// Отфильтровать события по дате события. Оставляет события в указанную дату.
        /// </summary>
        public EventsSourceBuilder FilterEventsDate(DateTime eventDate)
        {
            _result = _result.Where(w => w.Date.Date.Equals(eventDate.Date)).ToList();

            return this;
        }

        /// <summary>
        /// Отфильтровать события по доступности, оставляет те, на которые можно записаться.
        /// </summary>
        public EventsSourceBuilder FilterAvailable(SPUser user)
        {
            List<EventViewModel> result = new List<EventViewModel>();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            foreach (EventViewModel eventModel in _result)
            {
                if (eventModel.Times.IsNotEmpty() && !eventModel.EventParticipants.IsNullOrEmpty())
                {
                    EventParticipantModel[] existingParticipants = serializer.Deserialize<EventParticipantModel[]>(eventModel.EventParticipants);
                    if (existingParticipants.IsNotEmpty())
                    {
                        if (existingParticipants.Any(x => x.UserID == user.ID))
                        {
                            eventModel.Times.Clear();
                        }
                        else
                        {
                            eventModel.Times = eventModel.Times.Where(w => existingParticipants.Count(x => x.Time == w) < eventModel.MaxParticipants).ToList();
                        }
                    }
                }
                result.Add(eventModel);
            }

            _result = result;
            return this;
        }

        /// <summary>
        /// Получить список названий событий.
        /// </summary>
        public List<string> GetEventsTitles()
        {
            List<string> result = new List<string>();

            if (_result.IsNotEmpty())
            {
                result = _result.GroupBy(g => g.Title, StringComparer.OrdinalIgnoreCase).Select(s => s.Key).ToList();
            }

            return result;
        }

        /// <summary>
        /// Получить данные для списка дат.
        /// </summary>
        public List<ListItem> GetEventDates()
        {
            List<ListItem> result = new List<ListItem>();

            if (_result.IsNotEmpty())
            {
                result = _result.Select(s => s.Date).Distinct()
                    .Select(x => new ListItem(x.Date.ToString("dd.MM.yyyy"))).ToList();
            }

            return result;
        }

        /// <summary>
        /// Получить данные для списка времени.
        /// </summary>
        public List<ListItem> GetEventTimes()
        {
            List<ListItem> result = new List<ListItem>();

            if (_result.IsNotEmpty())
            {
                result = _result.SelectMany(s => s.Times).Distinct()
                    .Select(s => new ListItem(s.ToString(@"hh\:mm"), s.ToString())).ToList();
            }

            return result;
        }
        
        #endregion [methods]
    }
}
