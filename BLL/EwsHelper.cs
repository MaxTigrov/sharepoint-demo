﻿using Demo.EWS;
using Demo.Repositories.EventsDictionary;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.SharePoint;
using System;
using System.Globalization;

namespace Demo.BLL
{
    /// <summary>
    /// Хелпер для добавления события в календарь пользователя.
    /// </summary>
    internal class EwsHelper
    {
        #region [fields]

        private EventsRepository _eventsRepository;

        #endregion [fields]

        #region [properties]

        /// <summary>
        /// Текст ошибки.
        /// </summary>
        public string ErrorMessage { get; set; }

        #endregion [properties]

        public EwsHelper(EventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }

        #region [methods]

        /// <summary>
        /// Попытка добавить для пользователя событие в календарь outlook.
        /// </summary>
        public bool TryAddEventToCalendar(string category, string title, string date, string time, SPUser user)
        {
            bool result = true;

            DateTime dateValue;
            TimeSpan timeValue = TimeSpan.Parse(time);
            DateTime.TryParseExact(date, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue);

            EventEntity entity = _eventsRepository.Get(category, title, dateValue, timeValue);
            if (entity != null)
            {
                DateTime enentDate = dateValue.Date.Add(timeValue);
                string eventTitle = string.Format(@"{0}_{1}_{2:dd MMMM yyyy}_{3}", entity.EventCategory, entity.EventTitle, dateValue, timeValue.ToString(@"hh\:mm"));
                result = AddEventToCalendar(enentDate, eventTitle, user);
            }
            else
            {
                result = false;
                ErrorMessage = "Мероприятие, с выбранными параметрами не найдено. Попробуйте обновить страницу";
            }

            return result;
        }

        private bool AddEventToCalendar(DateTime eventDateTime, string eventTitle, SPUser user)
        {
            bool result = true;

            ExchangeServiceManager manager = new ExchangeServiceManager();
            AppointmentBuilder builder = new AppointmentBuilder(manager.Service);

            Appointment appointment = builder.Reset().SetSubject(eventTitle)
                .SetBody("").SetStartDateTime(eventDateTime)
                .SetEndDateTime(eventDateTime.AddHours(1)).Result();

            if (!manager.SendApointment(appointment))
            {
                result = false;
                ErrorMessage = manager.ErrorMessage;
            }

            return result;
        }

        #endregion [methods]
    }
}
