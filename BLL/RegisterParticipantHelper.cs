﻿using Demo.Models;
using Demo.Repositories.EventsDictionary;
using Microsoft.SharePoint;
using SP.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;

namespace Demo.Users
{
    /// <summary>
    /// Хелпер для проведения процесса регистрации участника на событие.
    /// </summary>
    internal class RegisterParticipantHelper
    {
        #region [fields]

        private static object _locker = new object();
        private EventsRepository _eventsRepository;

        #endregion [fields]

        #region [properties]

        /// <summary>
        /// Текс сообщения ошибки.
        /// </summary>
        public string ErrorMessage { get; private set; }

        #endregion [properties]

        public RegisterParticipantHelper(EventsRepository eventsRepository)
        {
            _eventsRepository = eventsRepository;
        }

        #region [methods]

        /// <summary>
        /// Регистрация участника на событие.
        /// </summary>
        /// <returns>True если удалось зарегистрировать сотрудника. False если есть ошибка. Текст ошибки в поле <see cref="ErrorMessage"/>.</returns>
        public bool TryRegister(string category, string title, string date, string time, SPUser user)
        {
            bool result = true;

            lock (_locker)
            {
                DateTime dateValue;
                TimeSpan timeValue = TimeSpan.Parse(time);
                DateTime.TryParseExact(date, "dd.MM.yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateValue);

                EventEntity entity = _eventsRepository.Get(category, title, dateValue, timeValue);
                if (entity != null)
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    if (entity.EventParticipants.IsNullOrEmpty())
                    {
                        // Первая заявка. Просто добавляем запись.
                        List<EventParticipantModel> info = new List<EventParticipantModel>();
                        info.Add(new EventParticipantModel
                        {
                            Time = timeValue,
                            UserID = user.ID
                        });

                        entity.EventParticipants = serializer.Serialize(info);
                        _eventsRepository.Update(entity);
                    }
                    else
                    {   
                        List<EventParticipantModel> info = serializer.Deserialize<List<EventParticipantModel>>(entity.EventParticipants);
                        // Проверяем есть ли доступное время для записи
                        if (info.Count >= entity.MaxParticipants)
                        {
                            ErrorMessage = "На это мероприятие записано максимальное кол-во участников. Выберите другое время";
                            result = false;
                        }
                        else
                        {
                            // Проверяем не пытается ли пользователь второй раз записаться
                            if (info.Any(x => x.Time == timeValue && x.UserID == user.ID))
                            {
                                ErrorMessage = "Вы уже записаны на это событие";
                                result = false;
                            }
                            else
                            {
                                // Все нормально-записываем пользователя.
                                info.Add(new EventParticipantModel
                                {
                                    Time = timeValue,
                                    UserID = user.ID
                                });

                                entity.EventParticipants = serializer.Serialize(info);
                                _eventsRepository.Update(entity);
                            }
                        }
                    }
                }
                else
                {
                    ErrorMessage = "Мероприятие, с выбранными параметрами не найдено. Попробуйте обновить страницу";
                    result = false;
                }
            }
            return result;
        }

        #endregion [methods]
    }
}
