﻿using Demo.Models;
using Microsoft.SharePoint;
using SP.Core.Users;
using System;

namespace Demo.BLL
{
    /// <summary>
    /// Хелпер для работы с информацией пользователя.
    /// </summary>
    internal class UserInfoHelper
    {
        #region [fields]

        private IUserInfoProvider _userInfoProvider;

        #endregion [fields]

        public UserInfoHelper(IUserInfoProvider userInfoProvider)
        {
            _userInfoProvider = userInfoProvider;
        }

        #region [methods]

        /// <summary>
        /// Получить информацию о пользователе.
        /// </summary>
        public UserInfoViewModel Get(SPUser user)
        {
            UserInfoViewModel result = new UserInfoViewModel();

            IUserInfo userInfo = _userInfoProvider.Get(user.LoginName);
            if (userInfo == null)
            {
                result.IsExists = false;
            }
            else
            {
                result.IsExists = true;
                result.FullName = userInfo.GetProperty("PreferredName").ToString();
                result.Department = userInfo.Department;
                object hireDate = userInfo.GetProperty("SPS-HireDate");
                if (hireDate != null)
                {
                    result.HireDate = ((DateTime)hireDate);
                }
            }

            return result;
        }

        /// <summary>
        /// Проверить доступна ли запись на мероприятия для этого пользователя.
        /// </summary>
        public bool AllowRegistration(UserInfoViewModel model)
        {
            bool result = true;
            // Проверяем по дате найма, если работает дольше 3х месяцев(90 дней) то разрешаем регистрацию.
            if (model.HireDate.HasValue && (DateTime.Now - model.HireDate.Value).TotalDays < 90)
            {
                result = false;
            }

            return result;
        }

        #endregion [methods]
    }
}
