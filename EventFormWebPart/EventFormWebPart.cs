﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls.WebParts;

namespace Demo.EventFormWebPart
{
    /// <summary>
    /// Вебчасть для регистрации на мероприятие.
    /// </summary>
    [ToolboxItemAttribute(false)]
    public class EventFormWebPart : WebPart
    {
        #region [methods]

        protected override void CreateChildControls()
        {
            // Показываем только при режиме просмотра.
            if (SPContext.Current.FormContext.FormMode == SPControlMode.Display)
            {
                Control control = Page.LoadControl("~/_controltemplates/15/PeterDemo/EventForm.ascx");
                if (control != null)
                {
                    Controls.Add(control);
                }
            }
        }

        #endregion [methods]
    }
}
