﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EventForm.ascx.cs" Inherits="Demo.ControlTemplates.EventForm" %>

<table class="ms-formtable" style="margin-top: 8px;" border="0" cellpadding="0" cellspacing="0">
    <tbody>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>ФИО</nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:Literal runat="server" ID="LitFullName" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Подразделение</nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:Literal runat="server" ID="LitDepartment" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Дата начала работы</nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:Literal runat="server" ID="LitHireDate" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Категория мероприятия<span class="ms-accentText" title="Это поле является обязательным."> *</span></nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:DropDownList runat="server" ID="DrlCategories" AutoPostBack="true"
                        OnSelectedIndexChanged="DrlCategories_SelectedIndexChanged" Width="100%" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Мероприятие<span class="ms-accentText" title="Это поле является обязательным."> *</span></nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:DropDownList runat="server" ID="DrlEvents" AutoPostBack="true"
                        OnSelectedIndexChanged="DrlEvents_SelectedIndexChanged" Enabled="false" Width="100%" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Дата мероприятия<span class="ms-accentText" title="Это поле является обязательным."> *</span></nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:DropDownList runat="server" ID="DrlEventDate" AutoPostBack="true" DataTextField="Text" DataValueField="Value"
                        OnSelectedIndexChanged="DrlEventDate_SelectedIndexChanged" Enabled="false" Width="100%" />
                </span>
            </td>
        </tr>
        <tr>
            <td nowrap="true" style="vertical-align: top; width: 200px;" class="ms-formlabel">
                <h3 class="ms-standardheader">
                    <nobr>Время мероприятия<span class="ms-accentText" title="Это поле является обязательным."> *</span></nobr>
                </h3>
            </td>
            <td style="vertical-align: top; width: 300px;" class="ms-formbody">
                <span dir="none">
                    <asp:DropDownList runat="server" ID="DrlEventTime" AutoPostBack="true" DataTextField="Text" DataValueField="Value"
                        OnSelectedIndexChanged="DrlEventTime_SelectedIndexChanged" Enabled="false" Width="100%" />
                </span>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right;">
                <asp:Button runat="server" ID="BtnSave" Text="Сохранить" OnClick="BtnSave_Click" Enabled="false" />
            </td>
        </tr>
    </tbody>
</table>

<script>
    function success(msg) {
        window.setTimeout(function () {
            var statusId = SP.UI.Status.addStatus(msg);
            SP.UI.Status.setStatusPriColor(statusId, 'green');
        }, 500);
    }

    function error(msg) {
        window.setTimeout(function () {
            var statusId = SP.UI.Status.addStatus(msg);
            SP.UI.Status.setStatusPriColor(statusId, 'red');
        }, 500);
    }
</script>
