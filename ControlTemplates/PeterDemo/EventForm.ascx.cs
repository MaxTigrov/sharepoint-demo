﻿using Demo.Users;
using Demo.Repositories.EventsDictionary;
using SP.Core;
using SP.Core.Users;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Demo.BLL;
using Demo.Models;

namespace Demo.ControlTemplates
{
    /// <summary>
    /// Контрол вывода формы регистрации.
    /// </summary>
    public partial class EventForm : SP.Core.UI.SPCoreControl
    {
        #region [fields]

        private EventsRepository _eventsRepa;

        private IUserInfoProvider _userInfoProvider;

        #endregion [fields]

        #region [properties]

        /// <summary>
        /// Инстанс репы справочника событий.
        /// </summary>
        protected EventsRepository EventsRepa
        {
            get { return _eventsRepa ?? (_eventsRepa = new EventsRepository(ContextWeb)); }
        }

        /// <summary>
        /// Провайдер получения информации о пользователях.
        /// </summary>
        protected IUserInfoProvider UserInfoProvider
        {
            get { return _userInfoProvider ?? (_userInfoProvider = new ProfileUsersProvider(ContextWeb)); }
        }

        #endregion [properties]

        #region [actions]

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                DataBind();
        }

        protected void DrlCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEvents(DrlCategories.SelectedValue);
        }

        protected void DrlEvents_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDates(DrlCategories.SelectedValue, DrlEvents.SelectedValue);
        }

        protected void DrlEventDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime selectedDate;
            if (DateTime.TryParse(DrlEventDate.SelectedValue, out selectedDate))
            {
                BindTimes(DrlCategories.SelectedValue, DrlEvents.SelectedValue, selectedDate);
            }
            else
            {
                BindTimes();
            }
        }

        protected void DrlEventTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            BtnSave.Enabled = !DrlEventTime.SelectedValue.IsNullOrEmpty();
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        #endregion [actions]

        #region [methods]

        public override void DataBind()
        {
            bool userInfoValid = BindUserInfo();
            BindCategories(userInfoValid);
            BindEvents();
            BindDates();
            BindTimes();
        }

        private bool BindUserInfo()
        {
            bool result = true;

            UserInfoHelper helper = new UserInfoHelper(UserInfoProvider);

            UserInfoViewModel userInfo = helper.Get(ContextWeb.CurrentUser);
            if (!userInfo.IsExists)
            {
                result = false;
            }
            else
            {
                LitFullName.Text = userInfo.FullName;
                LitDepartment.Text = userInfo.Department;
                if (userInfo.HireDate.HasValue)
                {
                    LitHireDate.Text = userInfo.HireDate.Value.ToString("dd MMMM yyyy");
                    if (!helper.AllowRegistration(userInfo))
                    {
                        SetErrorMessage("Регистрация недоступна до окончания испытательного срока");
                        result = false;
                    }
                }
            }

            return result;
        }

        private void SetErrorMessage(string messageText)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), messageText.GetHashCode().ToString(), $"error('{messageText}');", true);
        }

        private void SetConfirmMessage(string messageText)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), messageText.GetHashCode().ToString(), $"success('{messageText}');", true);
        }

        private bool BindCategories(bool enabled = true)
        {
            bool result = true;

            DrlCategories.ClearSelection();

            if (enabled == false)
            {
                DrlCategories.Items.Clear();
                DrlCategories.Enabled = false;
                result = false;
            }
            else
            {
                DrlCategories.Enabled = true;
                DrlCategories.DataSource = EventsRepa.GetEventCategories();
                DrlCategories.DataBind();

                if (DrlCategories.Items.Count > 0)
                {
                    DrlCategories.Items.Insert(0, new ListItem("Выберите категорию", ""));
                }
                else
                {
                    SetErrorMessage("Нет категорий для выбора");
                    result = false;
                }
            }

            return result;
        }

        private bool BindEvents(string category = null)
        {
            bool result = true;

            DrlEvents.ClearSelection();
            BindDates();

            if (category.IsNullOrEmpty())
            {
                DrlEvents.Items.Clear();
                result = false;
            }
            else
            {
                EventsSourceBuilder builder = new EventsSourceBuilder(EventsRepa);
                DrlEvents.DataSource = builder.GetEventsByCategory(category).FilterEventsDateTime().GetEventsTitles();
                DrlEvents.DataBind();

                if (DrlEvents.Items.Count > 0)
                {
                    DrlEvents.Items.Insert(0, new ListItem("Выберите мероприятие", ""));
                }
                else
                {
                    SetErrorMessage("Нет доступных мероприятий для выбранной категории");
                    result = false;
                }
            }

            DrlEvents.Enabled = result;
            return result;
        }

        private bool BindDates(string category = null, string eventTitle = null)
        {
            bool result = true;

            DrlEventDate.ClearSelection();
            BindTimes();

            if (category.IsNullOrEmpty() || eventTitle.IsNullOrEmpty())
            {
                DrlEventDate.Items.Clear();
                result = false;
            }
            else
            {
                EventsSourceBuilder builder = new EventsSourceBuilder(EventsRepa);
                DrlEventDate.DataSource = builder.GetEventsByCategory(category).FilterEventsDateTime()
                    .FilterEventsTitle(eventTitle).GetEventDates();
                DrlEventDate.DataBind();

                if (DrlEventDate.Items.Count > 0)
                {
                    DrlEventDate.Items.Insert(0, new ListItem("Выберите дату", ""));
                }
                else
                {
                    SetErrorMessage("Нет доступных дат для выбранного мероприятия");
                    result = false;
                }
            }

            DrlEventDate.Enabled = result;

            return result;
        }

        private bool BindTimes(string category = null, string eventTitle = null, DateTime? eventDate = null)
        {
            bool result = true;

            DrlEventTime.ClearSelection();

            if (category.IsNullOrEmpty() || eventTitle.IsNullOrEmpty() || !eventDate.HasValue)
            {
                DrlEventTime.Items.Clear();
                result = false;
            }
            else
            {
                EventsSourceBuilder builder = new EventsSourceBuilder(EventsRepa);
                DrlEventTime.DataSource = builder.GetEventsByCategory(category).FilterEventsDateTime()
                    .FilterEventsTitle(eventTitle).FilterEventsDate(eventDate.Value)
                    .FilterAvailable(ContextWeb.CurrentUser).GetEventTimes();
                DrlEventTime.DataBind();

                if (DrlEventTime.Items.Count > 0)
                {
                    DrlEventTime.Items.Insert(0, new ListItem("Выберите время", ""));
                }
                else
                {
                    SetErrorMessage("Нет доступных вариантов времени для выбранного мероприятия");
                    result = false;
                }
            }

            DrlEventTime.Enabled = result;

            return result;
        }

        private void Save()
        {
            RegisterParticipantHelper registerHelper = new RegisterParticipantHelper(EventsRepa);
            if (registerHelper.TryRegister(DrlCategories.SelectedValue, DrlEvents.SelectedValue, DrlEventDate.SelectedValue, DrlEventTime.SelectedValue, ContextWeb.CurrentUser))
            {
                SetConfirmMessage("Вы успешно записаны на мероприятие");
                MailHelper mailHelper = new MailHelper(EventsRepa);
                if(mailHelper.TrySendEmail(DrlCategories.SelectedValue, DrlEvents.SelectedValue, DrlEventDate.SelectedValue, DrlEventTime.SelectedValue, ContextWeb.CurrentUser))
                {
                    SetConfirmMessage("Уведомление отправлено на почту");
                }
                else
                {
                    SetErrorMessage($"Ошибка отправки уведомления: {mailHelper.ErrorMessage}");
                }
                ////EwsHelper ewsHelper = new EwsHelper(EventsRepa);
                ////if(ewsHelper.TryAddEventToCalendar(DrlCategories.SelectedValue, DrlEvents.SelectedValue, DrlEventDate.SelectedValue, DrlEventTime.SelectedValue, ContextWeb.CurrentUser))
                ////{
                ////    SetConfirmMessage("Событие добавлено в календарь.");
                ////}
                ////else
                ////{
                ////    SetErrorMessage($"Ошибка календаря: {ewsHelper.ErrorMessage}");
                ////}
            }
            else
            {
                SetErrorMessage($"Ошибка регистрации: {registerHelper.ErrorMessage}");
            }
        }

        #endregion [methods]
    }
}
