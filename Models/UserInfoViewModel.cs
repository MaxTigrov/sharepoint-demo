﻿using System;

namespace Demo.Models
{
    /// <summary>
    /// Модель представления данных пользователя.
    /// </summary>
    public class UserInfoViewModel
    {
        #region [properties]

        /// <summary>
        /// ФИО.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Департамент.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Дата найма.
        /// </summary>
        public DateTime? HireDate { get; set; }

        /// <summary>
        /// Пользователь существует.
        /// </summary>
        public bool IsExists { get; set; }

        #endregion [properties]
    }
}
