﻿using System;
using System.Collections.Generic;

namespace Demo.Models
{
    /// <summary>
    /// Модель представления для формы регистрации.
    /// </summary>
    [Serializable]
    internal sealed class EventViewModel
    {
        #region [properties]

        /// <summary>
        /// ID события.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Название события.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Дата события.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Время, в которое можно записаться.
        /// </summary>
        public List<TimeSpan> Times { get; set; }

        /// <summary>
        /// Список записанных участников на это событие. Ввиде Json.
        /// </summary>
        public string EventParticipants { get; set; }

        /// <summary>
        /// Максимальное кол-во записавшихся на один интервал времени.
        /// </summary>
        public int MaxParticipants { get; set; }

        #endregion [properties]
    }
}
