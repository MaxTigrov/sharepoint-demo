﻿using System;
using System.Web.Script.Serialization;

namespace Demo.Models
{
    /// <summary>
    /// Модель для сериализации данных об участниках мероприятий.
    /// </summary>
    [Serializable]
    public class EventParticipantModel
    {
        #region [properties]

        /// <summary>
        /// ID пользователя, записавшегося на событие.
        /// </summary>
        public int UserID { get; set; }

        /// <summary>
        /// Строковое представление времени.
        /// </summary>
        public string TimeStr { get; set; }

        /// <summary>
        /// Время записи.
        /// </summary>
        [ScriptIgnore]
        public TimeSpan Time
        {
            get
            {
                TimeSpan result;
                TimeSpan.TryParse(TimeStr, out result);
                return result;
            }
            set
            {
                TimeStr = value.ToString();
            }
        }

        #endregion [properties]
    }
}
