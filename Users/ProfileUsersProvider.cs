﻿using Microsoft.Office.Server.UserProfiles;
using Microsoft.SharePoint;
using SP.Core;
using SP.Core.Helpers;
using SP.Core.Users;
using SP.Core.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Demo.Users
{
    public class ProfileUsersProvider : IUserInfoProvider
    {
        #region [properties]

        public virtual string DisplayName
        {
            get { return "Служба профилей"; }
        }

        internal protected SPWeb Web
        {
            get { return _web; }
            private set { _web = value; }
        }
        private SPWeb _web;

        protected PropertiesKeyCollection _propertiesKeys = new PropertiesKeyCollection();

        public virtual IUserPhotoProvider PhotoProvider
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion [properties]

        public ProfileUsersProvider()
            : this(SPContext.Current.Web)
        {

        }

        public ProfileUsersProvider(SPWeb web)
        {
            if (web == null)
            {
                throw new ArgumentNullException("web");
            }

            Web = web;
            SetExtractProperties(new PropertiesKeyCollection() { "SPS-HireDate", "PreferredName" });
        }

        #region [methods]

        public IUserInfo Get(int spUserID)
        {
            SPUser user = UsersUtility.GetUser(Web, spUserID);
            if (user != null)
                return Get(user.LoginName);
            else
                return null;
        }

        public IUserInfo Get(string login)
        {
            if (string.IsNullOrEmpty(login))
                throw new ArgumentNullException("login");

            IUserInfo ret = GetAll().FirstOrDefault(w => LoginHelper.Comparer.Equals(login, w.Login));
            return ret;
        }

        public virtual IEnumerable<IUserInfo> GetAll()
        {
            using (new SPWebHelper(Web))
            {
                using (new SPSecurity.GrantAdditionalPermissionsInScope(SPBasePermissions.FullMask))
                {
                    SPServiceContext serviceContext = SPServiceContext.GetContext(Web.Site);
                    UserProfileManager profileManager = new UserProfileManager(serviceContext);
                    foreach (UserProfile profile in profileManager)
                    {
                        if (profile != null)
                        {
                            IUserInfo ret = ProfileUserInfo.Get(Web, profile, _propertiesKeys);
                            if (ret != null)
                                yield return ret;
                        }
                    }
                }
            }
        }

        public IEnumerable<IUserInfo> GetByFilter(IUserProviderFilter filter)
        {
            ArgumentsUtility.ThrowIfNull(filter, "filter");
            ArgumentsUtility.ThrowIfNull(filter.Filter, "filter.Filter");

            var query = GetAll().Where(filter.Filter);
            foreach (IUserInfo info in query)
                yield return info;
        }

        public virtual void SetExtractProperties(PropertiesKeyCollection propertiesKeys)
        {
            if (propertiesKeys.IsNotEmpty())
            {
                foreach (var prop in propertiesKeys)
                {
                    if (!_propertiesKeys.Contains(prop, StringComparer.OrdinalIgnoreCase))
                    {
                        _propertiesKeys.Add(prop);
                    }
                }
            }
        }

        public void Update(IUserInfo userInfo)
        {
            if (userInfo == null)
                throw new ArgumentNullException("userInfo");

            using (new SPWebHelper(Web))
            {
                SPWebHelper.DoWithPoolAccount(Web, (s, w) =>
                {
                    SPServiceContext serviceContext = SPServiceContext.GetContext(s);
                    UserProfileManager profileManager = new UserProfileManager(serviceContext);
                    try
                    {
                        UserProfile profile = profileManager.GetUserProfile(userInfo.Login);
                        if (profile != null)
                        {
                            ProfileUserInfo.TrySetValue(profile, PropertyConstants.UserName, userInfo.Name);
                            ProfileUserInfo.TrySetValue(profile, PropertyConstants.WorkEmail, userInfo.Email);
                            ProfileUserInfo.TrySetValue(profile, PropertyConstants.JobTitle, userInfo.JobTitle);
                            ProfileUserInfo.TrySetValue(profile, PropertyConstants.Department, userInfo.Department);

                            if (_propertiesKeys.IsNotEmpty())
                                foreach (string key in _propertiesKeys.Distinct(StringComparer.OrdinalIgnoreCase))
                                    ProfileUserInfo.TrySetValue(profile, key, userInfo.GetProperty(key));

                            profile.Commit();
                        }
                    }
                    catch (UserNotFoundException)
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                });
            }
        }

        #endregion [methods]
    }
}
