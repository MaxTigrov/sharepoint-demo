﻿using Microsoft.Office.Server.UserProfiles;
using Microsoft.SharePoint;
using SP.Core;
using SP.Core.Users;
using SP.Core.Utilities;
using System;
using System.Linq;

namespace Demo.Users
{
    /// <summary>
    /// Represents user information from SP pforiles.
    /// </summary>
    public class ProfileUserInfo : UserInfoTypeDescriptor, IUserInfo
    {
        #region [properties]

        public string Department { get; set; }

        public string Email { get; set; }

        public int ID { get; private set; }

        public string JobTitle { get; set; }

        public string Login { get; private set; }

        public string Name { get; set; }

        #endregion [properties]

        public static IUserInfo Get(SPWeb web, UserProfile profile, PropertiesKeyCollection propertiesKeys)
        {
            if (web == null)
            {
                throw new ArgumentNullException("web");
            }
            if (profile == null)
            {
                throw new ArgumentNullException("profile");
            }

            return new ProfileUserInfo(web, profile, propertiesKeys);
        }

        protected ProfileUserInfo(SPWeb web, UserProfile profile, PropertiesKeyCollection propertiesKeys)
        {
            ID = -1;
            Login = TryGetByKey<string>(profile, PropertyConstants.AccountName);
            //Login = LoginHelper.GetLoginWithDomain(TryGetByKey<string>(profile, PropertyConstants.Domain), Login);
            Name = TryGetByKey<string>(profile, PropertyConstants.UserName);
            Email = TryGetByKey<string>(profile, PropertyConstants.WorkEmail);
            JobTitle = TryGetByKey<string>(profile, PropertyConstants.JobTitle);
            Department = TryGetByKey<string>(profile, PropertyConstants.Department);

            if (propertiesKeys.IsNotEmpty())
                foreach (string key in propertiesKeys.Distinct(StringComparer.OrdinalIgnoreCase))
                {
                    object val = TryGetByKey<object>(profile, key);
                    SetProperty(key, val);
                }
        }

        #region [methods]

        public object GetProperty(string key)
        {
            return Properties.ContainsKey(key) ? Properties[key] : null;
        }

        public void SetProperty(string key, object value)
        {
            if (Properties.ContainsKey(key))
                Properties[key] = value;
            else
                Properties.Add(key, value);
        }

        internal static T TryGetByKey<T>(UserProfile profile, string key)
        {
            T ret = default(T);
            try
            {
                object val = profile[key].Value;
                if (val != null)
                    ret = (T)val;
            }
            catch (PropertyNotDefinedException ex)
            {
                DebugUtility.Fail(ex);
                System.Diagnostics.Debugger.Break();
            }
            return ret;
        }

        internal static bool TrySetValue(UserProfile profile, string key, object value)
        {
            try
            {
                profile[key].Value = value;
                return true;
            }
            catch (PropertyNotDefinedException)
            {
                return false;
            }
            catch (Exception ex)
            {
                DebugUtility.Fail(ex);
                System.Diagnostics.Debugger.Break();
                return false;
            }
        }

        #endregion [methods]
    }
}
